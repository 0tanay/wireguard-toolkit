# WireGuard® Toolkit

Generate configuration for all WireGuard peers from a single configuration file.

WGTK also generates a systemd service that you can use to make your configuration persistent.



## Quick start

Start from a template of the wgtk configuration:

	$ python3 -m wgtk template > config.yml

This will create a configuration file from the wgtk template. The documentation in the template should be sufficient for a basic configuration.

Ask wgtk to generate the configuration

	$ python3 -m wgtk generate config.yml

If there are no errors, you should get a folder structure like this:

	.
	├── andromeda
	│   ├── node01
	│   │   ├── control
	│   │   │   ├── install
	│   │   │   ├── start
	│   │   │   ├── stop
	│   │   │   └── wg-andromeda.service
	│   │   └── node01.conf
	│   └── node02
	│       ├── control
	│       │   ├── install
	│       │   ├── start
	│       │   ├── stop
	│       │   └── wg-andromeda.service
	│       └── node02.conf
	└── vpn.yml

Copy the configuration for each peer. The generated scripts expect the configuration to be placed under /etc/wireguard

	$ rsync andromeda/node01/ node01:/etc/wireguard/andromeda

The directory structure on peers should look like this:

	/etc/wireguard/andromeda
	├── control
	│   ├── install
	│   ├── start
	│   ├── stop
	│   └── wg-andromeda.service
	└── node01.conf

Then, for each peer:

	# cd /etc/wireguard
	# andromeda/control/install
	# systemctl enable wg-andromeda.service
	# systemctl start wg-andromeda.service



## Security

### wg executable

WGTK invokes `wg` to generate keys with the assumption that it resolves to the correct WireGuard executable.
This might not be true for your environment. When in doubt, it might be a good idea to check the full path of `wg`:

	$ which wg
	/usr/bin/wg

### Additional steps required
WGTK generates configuration sufficient to set up a WireGuard interface.
Additional steps may be required to secure your VPN. We highly recommend securing your VPN with a firewall.
